﻿using System;
using System.Threading.Tasks;
using Paillave.Etl.Core;
using Paillave.Etl.FileSystem;
using Paillave.Etl.TextFile;
using Paillave.Etl.Zip;
using TempEtl;
using TempEtl.Downloader;
using TempEtl.ElasticSearch;

public static class Program
{
    public static async Task Main(string[] args)
    {
        var processRunner = StreamProcessRunner.Create<string>(DefineProcess);
        var res = await processRunner.ExecuteAsync(null);
    }

    private static void DefineProcess(ISingleStream<string> contextStream)
    {
        contextStream
            .DownloadFile("download file from http",
                "https://data.nalog.ru/opendata/7707329152-envdr/data-20210329-structure-20200101.zip", 8)
            .CrossApplyFolderFiles("list all required files", x => x.DirectoryName, "*.zip")
            .CrossApplyZipFiles("extract files from zip", "*.csv")
            .CrossApplyTextFile("parse file",
                FlatFileDefinition.Create(i => new Person
                {
                    Id = i.ToColumn(0)
                }).IsColumnSeparated(';'))
            .ElasticSearchBulkIndex("bulk index to elastic", new ElasticSettings())
            .Do("Write to console", x=>Console.WriteLine(x.Id));
    }
}