using TempEtl.ElasticSearch;

namespace TempEtl
{
    public class ElasticSettings : IElasticSettings
    {
        public string[] ElasticIndexAddresses { get; set; } = new string[] { "http://dev-es.kadlab.local:9200" };

        public string IndexName { get; set; } = "temp_etl_net";

        public string IndexType { get; set; } = "person";
    }
}