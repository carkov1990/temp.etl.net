using System.IO;
using System.Threading.Tasks;

namespace TempEtl.Downloader;

public interface IHttpDownloader
{
    Task<FileInfo> DownloadFileAsync(string urlAddress);
    
    FileInfo DownloadFile(string urlAddress);
}