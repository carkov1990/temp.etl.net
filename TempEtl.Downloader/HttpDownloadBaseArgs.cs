using Paillave.Etl.Core;

namespace TempEtl.Downloader;

public class HttpDownloadBaseArgs : IHttpDownloadArgs
{
    private HttpDownloader _httpDownloader;
    public IStream<string> Stream { get; set; }

    public string UrlAddress { get; set; }
    
    public IHttpDownloader HttpDownloader { get; set; }

    public int ChunkSize { get; set; }

    public HttpDownloadBaseArgs(IStream<string> stream, string urlAddress, int chunkSize = 1)
    {
        Stream = stream;
        UrlAddress = urlAddress;
        ChunkSize = chunkSize;
        HttpDownloader = new HttpDownloader(chunkSize);
    }
}