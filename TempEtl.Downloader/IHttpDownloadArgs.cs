using Paillave.Etl.Core;

namespace TempEtl.Downloader;

public interface IHttpDownloadArgs
{
    IStream<string> Stream { get; set; }

    string UrlAddress { get; set; }

    IHttpDownloader HttpDownloader { get; set; }
}