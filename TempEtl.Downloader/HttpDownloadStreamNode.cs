using System.IO;
using System.Reflection.Metadata.Ecma335;
using Paillave.Etl.Core;
using Paillave.Etl.Reactive.Operators;

namespace TempEtl.Downloader;

public class HttpDownloadStreamNode : StreamNodeBase<FileInfo, IStream<FileInfo>, IHttpDownloadArgs>
{
    public HttpDownloadStreamNode(string name, IHttpDownloadArgs args) : base(name, args)
    {
    }

    protected override IStream<FileInfo> CreateOutputStream(IHttpDownloadArgs args)
    {
        return CreateUnsortedStream(args.Stream.Observable.Map(path => args.HttpDownloader.DownloadFile(args.UrlAddress)));
    }

    public override ProcessImpact PerformanceImpact { get; } = ProcessImpact.Light;
    public override ProcessImpact MemoryFootPrint { get; } = ProcessImpact.Light;
}