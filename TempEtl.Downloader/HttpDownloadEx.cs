﻿using System.IO;
using Paillave.Etl.Core;

namespace TempEtl.Downloader;

public static class HttpDownloadEx
{
    public static IStream<FileInfo> DownloadFile(this IStream<string> stream, string name, string urlAddress, IHttpDownloader downloader)
        => new HttpDownloadStreamNode(name, new HttpDownloadArgs()
        {
            Stream = stream,
            HttpDownloader = downloader,
            UrlAddress = urlAddress
        }).Output;

    public static IStream<FileInfo> DownloadFile(this IStream<string> stream, string name, string urlAddress)
        => DownloadFile(stream, name, urlAddress, 1);

    public static IStream<FileInfo> DownloadFile(this IStream<string> stream, string name, string urlAddress,
        int chunkCount)
        => new HttpDownloadStreamNode(name, new HttpDownloadBaseArgs(stream, urlAddress, chunkCount)).Output;
}