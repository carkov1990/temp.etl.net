using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Downloader;

namespace TempEtl.Downloader;

public class HttpDownloader : IHttpDownloader
{
    private DownloadConfiguration _downloadOptions;

    public HttpDownloader(DownloadConfiguration configuration)
    {
        _downloadOptions = configuration;
    }

    public HttpDownloader(int chunkCount)
    {
        _downloadOptions = new DownloadConfiguration()
        {
            BufferBlockSize = 8000, // usually, hosts support max to 8000 bytes, default values is 8000
            ChunkCount = chunkCount, // file parts to download, default value is 1
            MaximumBytesPerSecond = 1024 * 1024, // download speed limited to 1MB/s, default values is zero or unlimited
            MaxTryAgainOnFailover = int.MaxValue, // the maximum number of times to fail
            OnTheFlyDownload = false, // caching in-memory or not? default values is true
            ParallelDownload = chunkCount > 1, // download parts of file as parallel or not. Default value is false
            Timeout = 1000, // timeout (millisecond) per stream block reader, default values is 1000
            RequestConfiguration = // config and customize request headers
            {
                Accept = "*/*",
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate,
                CookieContainer = new CookieContainer(), // Add your cookies
                Headers = new WebHeaderCollection(), // Add your custom headers
                KeepAlive = false,
                ProtocolVersion = HttpVersion.Version11, // Default value is HTTP 1.1
                UseDefaultCredentials = false,
                UserAgent =
                    $"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36"
            }
        };
    }

    public async Task<FileInfo> DownloadFileAsync(string urlAddress)
    {
        var path = Path.Combine(Directory.GetCurrentDirectory(), "Downloads", Guid.NewGuid().ToString());
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }

        var downloader = new DownloadService(_downloadOptions);
        var directoryInfo = new DirectoryInfo(path);
        await downloader.DownloadFileTaskAsync(urlAddress, directoryInfo);

        return directoryInfo.GetFiles().FirstOrDefault();
    }

    public FileInfo DownloadFile(string urlAddress)
    {
        return DownloadFileAsync(urlAddress).Result;
    }
}