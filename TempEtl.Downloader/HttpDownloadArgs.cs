using Paillave.Etl.Core;

namespace TempEtl.Downloader;

public class HttpDownloadArgs : IHttpDownloadArgs
{
    public IStream<string> Stream { get; set; }
    public string UrlAddress { get; set; }
    public IHttpDownloader HttpDownloader { get; set; }
}