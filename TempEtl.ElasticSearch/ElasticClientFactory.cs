using System;
using System.Linq;
using Elasticsearch.Net;

namespace TempEtl.ElasticSearch
{
	/// <summary>
	/// Класс фабрики для получения клиент мониторингового elastic
	/// </summary>
	public class ElasticClientFactory
	{
		private readonly IElasticSettings _elasticSettings;
		private IElasticLowLevelClient _client;
		private readonly object _locker;

		/// <summary>
		/// .ctor
		/// </summary>
		public ElasticClientFactory(IElasticSettings elasticSettings)
		{
			_locker = new object();
			_elasticSettings = elasticSettings;
		}

		/// <inheritdoc cref="IElasticClientFactory.GetClient"/>
		public IElasticLowLevelClient GetClient()
		{
			if (_client == null)
			{
				lock (_locker)
				{
					if (_client == null)
					{
						_client = InitClient();
					}
				}
			}

			return _client;
		}

		private IElasticLowLevelClient InitClient()
		{
			var uri = _elasticSettings.ElasticIndexAddresses.First();
			var settings = new ConnectionConfiguration(new Uri(uri))
				.DisablePing().RequestTimeout(TimeSpan.FromSeconds(10));

			var elasticLowLevelClient = new ElasticLowLevelClient(settings);

			return elasticLowLevelClient;
		}
	}
}