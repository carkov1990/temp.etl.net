using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TempEtl.ElasticSearch;

public class BulkInsertService<TIn> where TIn : IElasticEntity
{
    private readonly ElasticClientFactory _clientFactory;
    private readonly IElasticSettings _elasticSettings;
    private IList<TIn> _entities = new List<TIn>();
    
    public BulkInsertService(IElasticSettings elasticSettings)
    {
        _elasticSettings = elasticSettings;
        _clientFactory = new ElasticClientFactory(elasticSettings);
    }

    public void Add(TIn entity)
    {
        _entities.Add(entity);
        if (_entities.Count == 200)
        {
            PutItemsAsync(_entities).Wait();
            _entities.Clear();
        }
    }

    public void Flush()
    {
        if (_entities.Count > 0)
        {
            PutItemsAsync(_entities).Wait();
            _entities.Clear();    
        }
    }
    
    private async Task PutItemsAsync(IEnumerable<TIn> items) 
    {
        var client = _clientFactory.GetClient();

        var response = await client.BulkAsync<byte[]>(
            items.SelectMany(x => new object[]
            {
                new
                {
                    index = new
                    {
                        _index = $"{_elasticSettings.IndexName}", _type = _elasticSettings.IndexType,
                        _id = x.Id
                    }
                },
                x
            }).ToArray());
    }
}