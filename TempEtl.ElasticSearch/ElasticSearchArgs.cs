using Paillave.Etl.Core;

namespace TempEtl.ElasticSearch;

public class ElasticSearchArgs<TIn> : IElasticSearchArgs<TIn>
{
    public IStream<TIn> Stream { get; set; }
    public IElasticSettings ElasticSettings { get; set; }
}