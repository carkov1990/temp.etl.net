namespace TempEtl.ElasticSearch
{
    /// <summary>
    /// Интерфейс настроек для ElasticSearch
    /// </summary>
    public interface IElasticSettings
    {
        /// <summary>
        /// Адрес сервера Elastic
        /// </summary>
        string[] ElasticIndexAddresses { get; set; }

        /// <summary>
        /// Название индекса
        /// </summary>
        public string IndexName { get; set; }

        /// <summary>
        /// Название типа данных
        /// </summary>
        public string IndexType { get; set; }
    }
}