﻿using Paillave.Etl.Core;
using Paillave.Etl.Reactive.Operators;

namespace TempEtl.ElasticSearch;

public class ElasticSearchBulkIndexStreamNode<TIn> : StreamNodeBase<TIn, IStream<TIn>, IElasticSearchArgs<TIn>>
    where TIn : IElasticEntity
{
    public ElasticSearchBulkIndexStreamNode(string name, IElasticSearchArgs<TIn> args) : base(name, args)
    {
    }

    protected override IStream<TIn> CreateOutputStream(IElasticSearchArgs<TIn> args)
    {
        var bulkIndexer = new BulkInsertService<TIn>(args.ElasticSettings);
        args.Stream.Observable.Subscribe(
            @in => { bulkIndexer.Add(@in); }, 
            () => { bulkIndexer.Flush(); });
        return CreateUnsortedStream(args.Stream.Observable);
    }


    public override ProcessImpact PerformanceImpact { get; }
    public override ProcessImpact MemoryFootPrint { get; }
}