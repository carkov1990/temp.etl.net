namespace TempEtl.ElasticSearch;

public interface IElasticEntity
{
    string Id { get; set; }
}