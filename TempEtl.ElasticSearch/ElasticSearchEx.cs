using Paillave.Etl.Core;

namespace TempEtl.ElasticSearch;

public static class ElasticSearchEx
{
    public static IStream<TIn> ElasticSearchBulkIndex<TIn>(this IStream<TIn> stream, string name, IElasticSettings settings) where TIn : IElasticEntity
    {
        return new ElasticSearchBulkIndexStreamNode<TIn>(name, new ElasticSearchArgs<TIn>()
        {
            Stream = stream,
            ElasticSettings = settings
        }).Output;
    }
}