using Paillave.Etl.Core;

namespace TempEtl.ElasticSearch;

public interface IElasticSearchArgs<TIn>
{
    IStream<TIn> Stream { get; set; }
    IElasticSettings ElasticSettings { get; set; }
}